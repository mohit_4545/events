//
//  ViewController.swift
//  Events
//
//  Created by Mohit Gupta on 21/08/1942 Saka.
//

import UIKit


//var EventList: [Event] = []
var currentIndex = 0
var fm = FileManager.default
var fresult: Bool = false
var subUrl: URL?
var mainUrl: URL? = Bundle.main.url(forResource: "mock", withExtension: "json")

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
 
        // Call method to get json data from document directory
        getData()
    }
   
}

// The extension has a method of obtaining data from a json file that is located in the principle directory

extension ViewController{
    func getData() {
        do {
            let documentDirectory = try fm.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: false)
            subUrl = documentDirectory.appendingPathComponent("mock.json")
            loadFile(mainPath: mainUrl!, subPath: subUrl!)
        } catch {
            print(error)
        }
    }
    
    func loadFile(mainPath: URL, subPath: URL){
        if fm.fileExists(atPath: subPath.path){
            decodeData(pathName: subPath)
            
            if EventList.isEmpty{
                decodeData(pathName: mainPath)
            }
            
        }else{
            decodeData(pathName: mainPath)
        }
         
    }
    
    func decodeData(pathName: URL){
        do{
            let jsonData = try Data(contentsOf: pathName)
            let decoder = JSONDecoder()
            EventList = try decoder.decode([Event].self, from: jsonData)
        } catch {}
    }
}
