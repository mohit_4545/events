//
//  EventListViewController.swift
//  Events
//
//  Created by Mohit Gupta on 21/08/1942 Saka.
//

import UIKit

// variable for contain event list
var EventList: [Event] = []
var EventInteractionList: [Event] = []
var GenricEventList: [Event] = []

class EventListViewController: UIViewController {
    
    fileprivate let CellReuseIdentifier = "TableViewCellIdentifier"
    fileprivate let tableview = UITableView()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.backgroundColor = UIColor.init(red: 0.2431372549, green: 0.7647058824, blue: 0.8392156863, alpha: 1)
        
        
        // initial method for setup userinterface and get data
        
        setUpNavigation()
        configureTableView()
        decodeData()
        
        GenricEventList = EventList
         
    }
    
    func setUpNavigation() {
        
        navigationItem.title = "Events"
        self.navigationController?.navigationBar.barTintColor = UIColor.init(red: 0.2431372549, green: 0.7647058824, blue: 0.8392156863, alpha: 1)
        self.navigationController?.navigationBar.isTranslucent = false
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.init(red: 1, green: 1, blue: 1, alpha: 1)]
         
         

        navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .refresh, target: self, action: #selector(addTapped))
    }
     
    
    func configureTableView() {
        tableview.dataSource = self
        tableview.estimatedRowHeight = 100
        tableview.rowHeight = UITableView.automaticDimension
        tableview.register(CustomTableViewCell.self, forCellReuseIdentifier: CellReuseIdentifier)
        tableview.showsVerticalScrollIndicator = false
        view.addSubview(tableview)
        tableview.translatesAutoresizingMaskIntoConstraints = false
        tableview.topAnchor.constraint(equalTo: view.topAnchor).isActive = true
        tableview.leadingAnchor.constraint(equalTo: view.leadingAnchor).isActive = true
        tableview.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
        tableview.trailingAnchor.constraint(equalTo: view.trailingAnchor).isActive = true
    }
    
    @objc func addTapped(){
        GenricEventList = EventInteractionList
        self.tableview.reloadData()
    }
}

// Tableview data source and delegate method

extension EventListViewController: UITableViewDataSource,UITableViewDelegate{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        GenricEventList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
 
        let cell = tableview.dequeueReusableCell(withIdentifier: CellReuseIdentifier, for: indexPath) as! CustomTableViewCell
         
        cell.titleLabel.text = GenricEventList[indexPath.row].title
        cell.startLabel.text = "Start : \( GenricEventList[indexPath.row].start)"
        cell.endLabel.text = "End : \(GenricEventList[indexPath.row].end)"
         
        return cell
        
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 90
    }
    
}


extension EventListViewController{
    
    // Data encode and get conflicting events
    
    func decodeData(){
        EventInteractionList.removeAll()
        EventList.removeAll()
        let decoder = JSONDecoder()
        decoder.dateDecodingStrategy = .monthDayYearTime
        do {
            let events = try decoder.decode([Event].self, from: Data(json.utf8))
            print(events.sorted())
            EventList = events.sorted()
            
            // get conflicting Events
            let conflictingEvents: [(Event, Event)] = events.compactMap {
                for event in events where event != $0 {
                    if event.intersects(with: $0) && $0.end != event.start && event.end != $0.start {
                        
                        EventInteractionList.append($0)
                        return ($0, event)
                        
                    }
                }
                return nil
            }
            print(events.count)             // "21\n"
            print(conflictingEvents.count)  // "11\n"
        } catch {
            print(error)
        }
    }
     
}



