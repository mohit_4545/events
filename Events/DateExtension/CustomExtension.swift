//
//  CustomExtension.swift
//  Events
//
//  Created by Mohit Gupta on 21/08/1942 Saka.
//

import Foundation

// Use for date formatter
extension Formatter {
    static let custom: DateFormatter = {
        let formatter = DateFormatter()
        formatter.calendar = Calendar(identifier: .iso8601)
        formatter.locale = Locale(identifier: "en_US_POSIX")
        formatter.dateFormat = "MMMM d, yyyy h:mm a"
        return formatter
    }()
}


extension JSONDecoder.DateDecodingStrategy {
    static let monthDayYearTime = custom {
        let container = try $0.singleValueContainer()
        let string = try container.decode(String.self)
        guard let date = Formatter.custom.date(from: string) else {
            throw DecodingError.dataCorruptedError(in: container,
                  debugDescription: "Invalid date: " + string)
        }
        return date
    }
}

// Conflicting events that conflict with any other event

extension Event {
    var interval: DateInterval { .init(start: start, end: end) }
    func intersects(with event: Event) -> Bool { interval.intersects(event.interval) }
}


extension Event: Comparable {
    static func < (lhs: Event, rhs: Event) -> Bool { lhs.start < rhs.start }
}


extension Event: CustomStringConvertible {
    var description: String {
        "Title: \(title) - Start: \(Formatter.custom.string(from: start)) - End: \(Formatter.custom.string(from: end)) \n"
    }
}




