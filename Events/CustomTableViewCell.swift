//
//  CustomTableViewCell.swift
//  Events
//
//  Created by Mohit Gupta on 21/08/1942 Saka.
//

import UIKit

class CustomTableViewCell: UITableViewCell {
    
    let titleLabel = UILabel()
    let startLabel = UILabel()
    let endLabel = UILabel()

   
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    
    // MARK: Initalizers
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        let marginGuide = contentView.layoutMarginsGuide
        
        // configure titleLabel
        contentView.addSubview(titleLabel)
        titleLabel.translatesAutoresizingMaskIntoConstraints = false
        titleLabel.leadingAnchor.constraint(equalTo: marginGuide.leadingAnchor).isActive = true
        titleLabel.topAnchor.constraint(equalTo: marginGuide.topAnchor).isActive = true
        titleLabel.trailingAnchor.constraint(equalTo: marginGuide.trailingAnchor).isActive = true
        titleLabel.numberOfLines = 0
        titleLabel.font = UIFont(name: "AvenirNext-DemiBold", size: 16)
        
        // configure authorLabel
        contentView.addSubview(startLabel)
        startLabel.translatesAutoresizingMaskIntoConstraints = false
        startLabel.leadingAnchor.constraint(equalTo: marginGuide.leadingAnchor).isActive = true
//        startLabel.bottomAnchor.constraint(equalTo: marginGuide.bottomAnchor).isActive = true
        startLabel.trailingAnchor.constraint(equalTo: marginGuide.trailingAnchor).isActive = true
        startLabel.topAnchor.constraint(equalTo: titleLabel.bottomAnchor).isActive = true
        startLabel.numberOfLines = 0
        startLabel.font = UIFont(name: "Avenir-Book", size: 12)
        startLabel.textColor = UIColor.lightGray
        
        
        
        // configure authorLabel
        contentView.addSubview(endLabel)
        endLabel.translatesAutoresizingMaskIntoConstraints = false
        endLabel.leadingAnchor.constraint(equalTo: marginGuide.leadingAnchor).isActive = true
        endLabel.bottomAnchor.constraint(equalTo: marginGuide.bottomAnchor).isActive = true
        endLabel.trailingAnchor.constraint(equalTo: marginGuide.trailingAnchor).isActive = true
        endLabel.topAnchor.constraint(equalTo: startLabel.bottomAnchor).isActive = true
        endLabel.numberOfLines = 0
        endLabel.font = UIFont(name: "Avenir-Book", size: 12)
        endLabel.textColor = UIColor.lightGray
         
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
 
}
 
